#include <iostream>
#include <string>

using namespace std;

/* This is a simple example to test input validation. The user must create an account with an username and password. The username must be the student number.
 * This will determine if the username is of exactly 9 digits. It will also determine if the user enters a password
 * with the following characteristics.
 *      1. 8 or more characters of length
 *      2. At least 1 upper case letter
 *      3. At least 1 lower case letter
 *      4. Al least 1 symbol
 *      5. At least 1 digit
 * The program will not finish until the user enters a correct password and then re-enters the same password.
 */

 //Input validation for the username
bool isUsername(const string &username){
	// INSERT YOUR CODE HERE

	return 1 ; // CHANGE
}

//Input validation for the password
bool isPassword(const string &password){
	// INSERT YOUR CODE HERE
	
	return 1 ; // CHANGE
}

int main()
{

    string password, username, repassword;
    bool flaguser = false, flagpass = false;
    cout << "Please enter username (Student number, with or without dash): ";
    cin >> username;
    do{
        if(isUsername(username)){
            flaguser = true;
            cout << "The username is correct." << endl;
        }
        else{
            cout << "Please enter a correct username: ";
            cin >> username;
        }
    }while(!flaguser);

    cout << endl;
    cout << "Please enter a password with the following conditions: " << endl;
    cout << "\t1. 8 or more characters" << endl;
    cout << "\t2. At least 1 upper case letter" << endl;
    cout << "\t3. At least 1 digit" << endl;
    cout << "\t4. At least 1 symbol" << endl << endl;
    do{
        cout << "Please enter password: ";
        cin >> password;
        if(password.length() >= 8){
            if(isPassword(password)){
                cout << "Please reenter the password: ";
                cin >> repassword;
                if(password == repassword){
                    cout << "The password matched." << endl;
                    flagpass = true;
                }
                else {
                    cout << "The password did not matched." << endl;
                }
            }
            else{
                cout << "The password did not pass the conditions." << endl;
            }
        }
    }while(!flagpass);
    cout << "Congratulations, you have succesfully created an account!" << endl;

    return 0;
}

